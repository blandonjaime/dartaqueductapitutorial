import 'package:aqueduct/aqueduct.dart';
//import 'package:heroes/classes/heroes.dart';
import 'package:heroes/model/hero.dart';

class HeroesController extends ResourceController {
  HeroesController(this.context);

  final ManagedContext context;
  final _heroes = [
    {'id': 1, 'name': 'Bomberban'},
    {'id': 2, 'name': 'Mr. Nice'},
    {'id': 3, 'name': 'Superman'},
    {'id': 4, 'name': 'Batman'},
  ];

  @Operation.get()
  Future<Response> getAllHeroes( {@Bind.query('name') String name} ) async {
    final heroQuery = Query<Hero>(context);
    if( name != null){
       heroQuery.where((h) => h.name).contains(name, caseSensitive: false);
    }
    final heroes = await heroQuery.fetch();
    return Response.ok(heroes);
  }

  @Operation.get('id')
  Future<Response> getHeroByID(@Bind.path('id') int id) async {
    final heroQuery = Query<Hero>(context)..where((h) => h.id).equalTo(id);
    final hero = await heroQuery.fetchOne();

    if (hero == null) {
      return Response.notFound();
    }
    return Response.ok(hero);
  }

  @Operation.post()
  Future<Response> createHero() async {
    final Map<String, dynamic> body = await request.body.decode();
    
    final hero = Hero()
      ..read( await request.body.decode(), ignore: ['id'] );
    
    final query = Query<Hero>( context )
      ..values = hero;

    //final query = Query<Hero>( context )
    //..values.name = body['name'] as String;
    
    final insertedHero = await query.insert();
    return Response.ok( insertedHero );
  }

  /*
  Future<RequestOrResponse> handle(Request request) async {
    if (request.path.variables.containsKey('id')) {
      final id = int.parse(request.path.variables['id']);
      final hero =
          _heroes.firstWhere((hero) => hero['id'] == id, orElse: () => null);
      if (hero == null) {
        return Response.notFound();
      }

      return Response.ok(hero);
    }

    return Response.ok(_heroes);
  }
  */
}
